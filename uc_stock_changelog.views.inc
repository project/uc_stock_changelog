<?php
/**
 * @file
 * Informs views about the stock logging entries stored by uc_stock_changelog.
 */

/**
 * Implement hook_views_data().
 */
function uc_stock_changelog_views_data() {
  $data = array();

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['uc_stock_changelog_events']['table']['group'] = t('Ubercart Stock Log');

  // Advertise this table as a possible base table
  $data['uc_stock_changelog_events']['table']['base'] = array(
    'field' => 'event_id',
    'title' => t('Ubercart stock logs'),
    'help' => t('Historical product stock information.'),
  );

  // Pull in user fields directly.
  $data['users']['table']['join']['uc_stock_changelog_events'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
  );

  // Define fields
  $data['uc_stock_changelog_events']['event_id'] = array(
    'title' => t('Event ID'),
    'help' => t('Stock log ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['uc_stock_changelog_events']['created'] = array(
    'title' => t('Creation date'),
    'help' => t('The date and time the event occurred.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Expose the uid as a relationship.
  $data['uc_stock_changelog_events']['uid'] = array(
    'title' => t('User'),
    'help' => t('Relate an stock log event to the user who triggered it.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('customer'),
    ),
  );

  $data['uc_stock_changelog_events']['sku'] = array(
    'title' => t('SKU'),
    'help' => t('The product model/SKU.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['uc_stock_changelog_events']['field'] = array(
    'title' => t('Field Name'),
    'help' => t('The name of the field that was changed in the event.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['uc_stock_changelog_events']['value'] = array(
    'title' => t('Value'),
    'help' => t('The updated value of the field associated to of the event.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  $data['uc_stock_changelog_events']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description text.'),
    'field' => array(
      'handler' => 'views_handler_field_markup',
      'format' => 'filtered_html', // The name of the format field
      'click sortable' => FALSE,
    ),
  );

  return $data;
}
