<?php
/**
 * @file
 * Default view implementations for the uc_stock_changelog module.
 */

/**
 * Implements hook_views_default_views().
 */
function uc_stock_changelog_views_default_views() {
  $export = array();
  $export['admin_stock_changelog'] = uc_stock_changelog_default_admin_stock_changelog();
  return $export;
}

function uc_stock_changelog_default_admin_stock_changelog() {
  $view = new view();
  $view->name = 'admin_stock_changelog';
  $view->description = 'Displays historical information about stock levels for products.';
  $view->tag = 'default';
  $view->base_table = 'uc_stock_changelog_events';
  $view->human_name = 'Product Stock Change Log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Product Stock Change Log';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view reports';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No historical information is currently available. Product stock logging will begin the next time the product stock status, stock level or stock threshold are changed.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Ubercart Stock Log: Creation date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Date';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'User';
  /* Field: Ubercart Stock Log: SKU */
  $handler->display->display_options['fields']['sku']['id'] = 'sku';
  $handler->display->display_options['fields']['sku']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['fields']['sku']['field'] = 'sku';
  /* Field: Ubercart Stock Log: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Sort criterion: Ubercart Stock Log: Event ID */
  $handler->display->display_options['sorts']['event_id']['id'] = 'event_id';
  $handler->display->display_options['sorts']['event_id']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['sorts']['event_id']['field'] = 'event_id';
  $handler->display->display_options['sorts']['event_id']['order'] = 'DESC';
  /* Contextual filter: Ubercart Stock Log: SKU */
  $handler->display->display_options['arguments']['sku']['id'] = 'sku';
  $handler->display->display_options['arguments']['sku']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['arguments']['sku']['field'] = 'sku';
  $handler->display->display_options['arguments']['sku']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['sku']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['sku']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['sku']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['sku']['limit'] = '0';
  /* Filter criterion: Ubercart Stock Log: SKU */
  $handler->display->display_options['filters']['sku']['id'] = 'sku';
  $handler->display->display_options['filters']['sku']['table'] = 'uc_stock_changelog_events';
  $handler->display->display_options['filters']['sku']['field'] = 'sku';
  $handler->display->display_options['filters']['sku']['operator'] = 'starts';
  $handler->display->display_options['filters']['sku']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sku']['expose']['operator_id'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['label'] = 'SKU';
  $handler->display->display_options['filters']['sku']['expose']['operator'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['identifier'] = 'sku';
  $handler->display->display_options['filters']['sku']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/store/reports/stock_changelog';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Product Stock Change Log';
  $handler->display->display_options['menu']['description'] = 'Displays historical information about stock levels for products.';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  return $view;
}
