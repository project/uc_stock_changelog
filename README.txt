What does Ubercart Stock Changelog do?
--------------------------------------
This module allows site administrators to better manage their product stock by
having the site record all changes in product stock, both decrements (usually
from user orders) and increments (usually from restocks).

A stock changelog is provided under the Administration > Store > Reports
section to view the changes in product stock over time on a global or per-
product basis.


Requirements
------------
* PHP 5
* Ubercart's stock (uc_stock) module
* The patch provided in #1263546 (http://drupal.org/node/1263546) - as of
  writing, the most recent patch available was from comment #2:
  https://www.drupal.org/files/fire_hook_on_stock_change-1263546-5892320.patch


Configuration
-------------
Simply enable the module and begin managing product stock.

Whenever a product's stock administration form (the Stock tab on the
product node edit page) is submitted or whenever stock levels are modified, an
entry with the changes made to the product's stock configuration is entered into
the stock changelog.

The stock changelog report can be viewed at admin/store/reports/stock_changelog


Known issues and limitations
----------------------------
* There is no administration interface to configure the purge threshold. By
  default, stock changelog entries older than 1 year are purged from the
  database. This can be configured by modifying the variable
  uc_stock_changelog_purge_threshold.


Credits and Sponsors
--------------------
Development of this module was performed by Grindflow Management LLC,
http://www.grindflow.com.

This module was generously sponsored by Serialio.com, your mobile solution
provider on thousands of device models.
